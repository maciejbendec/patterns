
/**
  * Created by maciej on 28.04.17.
  */
import scala.util.matching.Regex

class ProductPrototype extends Cloneable
{
  override def clone() = super.clone
}

class CloneableProduct extends ProductPrototype
{
  var  myName : String = "placeholder"
  def setName(name: String) =
  {
    myName=name
  }
  def setName() : String =
  {
    myName
  }
  override def toString() : String =
  {
    "Nazwa produktu to: "+ myName
  }
}


abstract class Parser {
  def parse(filename: String)
}

/** Implements the algorithm using the strategy interface */
class JsonParser extends Parser {
  override def parse(filename: String){
  Console.println("Parsing JSON "+filename);
  }
}

class CsvParser extends Parser {
  override def parse(filename: String){
    Console.println("Parsing CSV "+filename);
  }
}
// Configured with a ConcreteStrategy object and maintains
// a reference to a Parser object
class Context(strategy : Parser) {
  def parse(filename: String){
    this.strategy.parse(filename)
  }
}


// test prototype
var product1 = new CloneableProduct()
var product2 = new CloneableProduct()
var product3 = product2.clone
println(product1)
println(product2)
product1.setName("Jeden")
product2.setName("Dwa")
println(product1)
println(product2)
//test context
val patternCsv = "csv"
val patternJson = "json"
val filename1="test1.csv"
val filename2="test2.json"
var parts = filename1.split("\\.")
if(parts(1).matches(patternCsv)){
  var context=new Context(new CsvParser)
  context.parse(filename1)
}
else{
  var context=new Context(new JsonParser)
  context.parse(filename1)
}
parts = filename2.split("\\.")
if(parts(1).matches(patternCsv)){
  var context=new Context(new CsvParser)
  context.parse(filename2)
}
else{
  var context=new Context(new JsonParser)
  context.parse(filename2)
}